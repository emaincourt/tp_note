$(document).ready(function() {
  $('#slider').change(function() {
    $('.slider-value').text($('#slider').val());
    filterByPrice($('#slider').val());
    checkExists();
  });

  $('#search').change(function() {
    $('.search-value').text($('#search').val() === '' ? 'Tous' : $('#search').val());
    filterBySearch($('#search').val());
    checkExists();
  });
})

/**
 * Hides the unmatching elements
 * @param {String} maxPrice 
 */
function filterByPrice(maxPrice) {
  const items = $('article');
  items.each(function () {
    const price = Number.parseFloat($(this).find('span.price').text());
    if (price < 0 || price > maxPrice) {
      $(this).hide();
    } else {
      $(this).show();
    }
  });
}

/**
 * Hides the unmatching elements
 * @param {String} search 
 */
function filterBySearch(search) {
  const items = $('article');
  items.each(function () {
    const match = $(this).find('h3').text().indexOf(search) !== -1 || search === '';
    if (!match) {
      $(this).hide();
    } else {
      $(this).show();
    }
  });
}

/**
 * Checks if some products should still be displayed
 */
function checkExists() {
  const hidden = $('article').filter(function () { 
    return this.style.display == 'none' ;
  });
  if (hidden.length >= $('article').length) {
    $('#products').append('<h3 id="no-result">Aucun résultat</h3>');
  } else {
    if ($("#no-result").length) {
      $('#no-result').remove();
    }
  }
}