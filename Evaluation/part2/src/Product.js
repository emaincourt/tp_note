import React, { Component } from 'react';

export default class Product extends Component {
  render() {
    return (
      <article className="row col-md-4 col-sm-6 br-10 b-grey mb-3">
        <figure className="col-md-3">
          <img src={this.props.image} alt={this.props.name}/>
        </figure>
        <figcaption className="ml-4 mt-3 mb-3 col-md-6 col-lg">
          <h3>{this.props.name}</h3>
          <p>{this.props.year} - <em>{this.props.color}</em></p>
          <p><i className="fa fa-eur" aria-hidden="true"></i><span className="price">${this.props.price}</span>€</p>
          <footer>
              <button className="btn btn-outline-primary mb-3"><i className="fa fa-bars mr-1" aria-hidden="true"></i>Description</button>
              <button className="btn btn-primary mb-3"><i className="fa fa-shopping-cart mr-1" aria-hidden="true"></i>Ajouter au panier</button>
          </footer>
        </figcaption>
      </article>
    )
  }
}