import React, { Component } from 'react';
import logo from './logo.svg';
import List from './List';
import Header from './Header';
import './App.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/font-awesome/css/font-awesome.min.css';

class App extends Component {
  constructor() {
    super()
    this.state = {
      maxPrice: 50,
      search: '',
    }
  }

  onMaxPriceChange(maxPrice) {
    this.setState({ maxPrice });
  }

  onSearchChange(search) {
    this.setState({ search });
  }

  render() {
    return (
      <div className="container-flex body">
        <Header
          maxPrice={this.state.maxPrice}
          search={this.state.search}
          onMaxPriceChange={(event) => this.onMaxPriceChange(event.target.value)}
          onSearchChange={(event) => this.onSearchChange(event.target.value)}
        />
        <main class="mt-3">
          <h2>Vins</h2>
          <p>
            <span className="search-value">
              {this.state.search !== '' ? this.state.search : 'Tous'}
            </span> - 0-{this.state.maxPrice}€ - 8 produits</p>
          <hr/>
          <section className="container-flex row justify-content-lg-between justify-content-md-between justify-content-sm-between justify-content-center" id="products">
            <List
              maxPrice={this.state.maxPrice}
              search={this.state.search}
            />
          </section>
        </main>
      </div>
    );
  }
}

export default App;
