import React, { Component } from 'react';
import Product from './Product';
import products from './products';

export default class List extends Component {
  render() {
    return products.filter(product => product.price <= this.props.maxPrice && product.title.includes(this.props.search)).map(product => (
      <Product
        image={product.img}
        name={product.title}
        year={product.year}
        color={product.type}
        price={product.price}
      />
    ))
  }
}