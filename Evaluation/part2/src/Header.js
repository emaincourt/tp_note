import React, { Component } from 'react';

export default class Header extends Component {
  render() {
    return (
      <header className="row p-3 h-100 grey-gradient shadow mr-0 ml-0">
        <figure className="col-md-2">
          <img src="./assets/logo.png" alt="Logo"/>
        </figure>
        <div className="input-group col-md-6 flex-column">
          <input type="text" placeholder="Rechercher un vin" id="search" value={this.props.search} onChange={this.props.onSearchChange} required autofocus className="form-control w-100 br-30"/>
          <label for="range" className="mb-0 mt-2">Prix maximum : {this.props.maxPrice}€</label>
          <input type="range" min="0" max="50" value={this.props.maxPrice} name="range" className="form-control w-100" id="slider" onChange={this.props.onMaxPriceChange}/>
        </div>
      </header>
    )
  }
}